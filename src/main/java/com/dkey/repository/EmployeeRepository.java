package com.dkey.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dkey.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
//    @Procedure(name = "addEmployeeThroughNamedStoredProcedureQuery")
//    ProcedureResult addEmployeeThroughNamedStoredProcedureQuery(@Param("FIRST_NAME") String firstName,
//                                                                @Param("LAST_NAME") String lastName,
//                                                                @Param("EMAIL") String email);
}
