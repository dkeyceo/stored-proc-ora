package com.dkey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoredProcOraApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoredProcOraApplication.class, args);
	}

}
